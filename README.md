# Contact Management Tool
The Contact Management Tool allows to manage contacts.

# Environment
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20 and Bootstrap 4.1.3

# Prerequisite
Node: 12.13.0

# Steps to set up project
1. cd <Project-Directory>
2. npm install

# Run project on local
1. ng serve --open 

# Application URL
http://localhost:4200/

# Run Unit test cases on local
1. ng test

# Project Structure
1. src/app/components : Contains Dumb components
2. src/app/constants/contents.ts: Contains the display content of the application at a common place
3. src/app/constants/validators.ts: Contains all the validators used 
4. src/app/models: Contains application models
5. src/app/pipes: Contains pipes used in application
6. src/app/services: Services used in the application
7. src/app/views: Contains views of the application

# Storage Used for Application
Browser's LocalStorage