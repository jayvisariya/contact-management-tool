import { Pipe, PipeTransform } from '@angular/core';
import { isNgTemplate } from '@angular/compiler';
import { IContact } from '../models/contact';

/**
 * Searches maching Contacts against search string
 */
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(list: IContact[], filterText: string): IContact[] {
    let fullName;
    return list ? list.filter(item => {
      fullName = item.firstName + ' ' + item.lastName;
      return (fullName.search(new RegExp(filterText, 'i'))) > -1
    }) : [];
  }
}
