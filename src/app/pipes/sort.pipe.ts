import { Pipe, PipeTransform } from '@angular/core';
import { IContact } from '../models/contact';

/**
 * Sorts the contacts based on first name.
 */
@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(list: IContact[], filterText: string): IContact[] {
    return list.sort((a, b) => {
      var x = a.firstName.toLowerCase();
      var y = b.firstName.toLowerCase();
      if (x < y) { return -1; }
      if (x > y) { return 1; }
      return 0;
    });
  }
}
