import { SortPipe } from './sort.pipe';
import { IContact } from '../models/contact';

describe('SortPipe', () => {
  it('create an instance', () => {
    const pipe = new SortPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return sorted contacts by first name', () => {
    const pipe = new SortPipe();
    let contacts: IContact[] = [
      {
        id: '1',
        firstName: 'Rahul',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }, {
        id: '2',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];
    const filteredContacts = pipe.transform(contacts, '');
    expect(filteredContacts[0].firstName).toEqual('Jay');
  });
});
