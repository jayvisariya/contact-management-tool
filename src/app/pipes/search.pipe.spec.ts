import { SearchPipe } from './search.pipe';
import { IContact } from '../models/contact';

describe('SearchPipe', () => {
  it('create an instance', () => {
    const pipe = new SearchPipe();
    expect(pipe).toBeTruthy();
  });
  it('should return matched contacts', () => {
    const pipe = new SearchPipe();
    let contacts: IContact[] = [{
      id: '1',
      firstName: 'Jay',
      lastName: 'Visariya',
      email: 'jayvisariya@gmail.com',
      phone: '8087556360',
      active: true
    },
    {
      id: '2',
      firstName: 'Rahul',
      lastName: 'Visariya',
      email: 'jayvisariya@gmail.com',
      phone: '8087556360',
      active: true
    }];
    const filteredContacts = pipe.transform(contacts,'Rahul');
    expect(filteredContacts[0].firstName).toEqual('Rahul');
  });
});
