import { Component, OnInit } from '@angular/core';
import { commonContent } from 'src/app/constants/content';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  content = {};

  constructor() { }
  
  ngOnInit() {
    this.content = commonContent;
  }
}
