import { ContactService } from './../../../services/contact.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { listContactContent } from 'src/app/constants/content';
import { IContact } from 'src/app/models/contact';

// declare var $:any;

@Component({
  selector: 'app-list-contact',
  templateUrl: './list-contact.component.html',
  styleUrls: ['./list-contact.component.css']
})
export class ListContactComponent implements OnInit {
  content: any = {};
  contacts: IContact[];

  constructor(private contactService: ContactService, private router: Router) { }

  ngOnInit() {
    this.content = listContactContent;
    this.contacts = this.contactService.getContacts('active');
  }

  /**
 * calls contactService.deleteContact to delete the contact
 * Parameter: IContact
 */
  deleteContact(contact) {
    this.contacts = this.contactService.deleteContact(contact).filter((contact) => contact.active == true);
  }

  /**
 * navigates to add-contact path with given id
 * Parameter: id
 */
  addContact(id) {
    this.router.navigate(['add-contact', id]);
  }

}
