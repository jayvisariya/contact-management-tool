import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListContactComponent } from './list-contact.component';
import { ContactService } from 'src/app/services/contact.service';
import { Router } from '@angular/router';
import { ContactDetailComponent } from 'src/app/components/contact-detail/contact-detail.component';
import { SortPipe } from 'src/app/pipes/sort.pipe';
import { SearchPipe } from 'src/app/pipes/search.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { IContact } from 'src/app/models/contact';

describe('ListContactComponent', () => {
  let component: ListContactComponent;
  let fixture: ComponentFixture<ListContactComponent>;
  let contactService: ContactService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListContactComponent, ContactDetailComponent, SortPipe, SearchPipe],
      imports: [RouterTestingModule.withRoutes([
        { path: 'add-contact/:id', component: ListContactComponent }
      ])]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListContactComponent);
    component = fixture.componentInstance;
    contactService = TestBed.get(ContactService);
    router = TestBed.get(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('addContact', () => {
    it('should navigate to add-contact view', () => {
      component.addContact(1);
      expect(component.addContact).toBeDefined();
    });
  });

  describe('deleteContact', () => {
    it('should call deleteCOntact method of the contact service', () => {
      const contact: IContact = {
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      };
      let contacts: IContact[] = [{
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      },
      {
        id: '2',
        firstName: 'Ajay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];

      spyOn(component['contactService'], 'deleteContact').and.callFake(function () {
        contacts[0].active = false;
        return contacts;
      });
      component.deleteContact(contact);
      expect(contacts[0].active).toEqual(false);
      expect(component['contactService'].deleteContact).toHaveBeenCalled();
    });
  });

});
