import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NAME_VALIDATOR, PHONE_VALIDATOR, EMAIL_VALIDATOR } from 'src/app/constants/validators';
import { addContactContent } from 'src/app/constants/content';
import { IContact } from 'src/app/models/contact';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {
  contactForm: FormGroup = this.formBuilder.group({
    firstName: ['', NAME_VALIDATOR],
    lastName: ['', NAME_VALIDATOR],
    email: ['', EMAIL_VALIDATOR],
    phone: ['', PHONE_VALIDATOR]
  });
  contact: IContact;
  feature: string = '';
  contactId: string = '';
  content: any = {};
  constructor(private formBuilder: FormBuilder, private contactService: ContactService, private router: Router, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.content = addContactContent;
    this.route.paramMap.subscribe(params => {
      this.feature = params.get('id') == 'new' ? 'Add' : 'Edit';
      this.contactId = params.get('id');
      if (this.feature == 'Add') {
        this.initializeForm(null);
      } else if (this.feature == 'Edit') {
        const contacts = this.contactService.getContacts('active');
        contacts.forEach((contact) => {
          if (contact.id == params.get('id')) {
            this.initializeForm(contact);
          }
        });
      }
    });
  }

  back() {
    this.location.back();
  }

  /**
   * Initialize the form
   */
  initializeForm(contact: IContact) {
    if (contact) {
      this.contactForm = this.formBuilder.group({
        firstName: [contact.firstName, NAME_VALIDATOR],
        lastName: [contact.lastName, NAME_VALIDATOR],
        email: [contact.email, EMAIL_VALIDATOR],
        phone: [contact.phone, PHONE_VALIDATOR]
      });
    } else {
      this.contactForm = this.formBuilder.group({
        firstName: ['', NAME_VALIDATOR],
        lastName: ['', NAME_VALIDATOR],
        email: ['', EMAIL_VALIDATOR],
        phone: ['', PHONE_VALIDATOR]
      });
    }
  }

/**
 * Prepares the contact data and call addContact method of contactService
 */
  addContact() {
    const contact: IContact = {
      id: '0',
      firstName: this.contactForm.controls.firstName.value,
      lastName: this.contactForm.controls.lastName.value,
      email: this.contactForm.controls.email.value,
      phone: this.contactForm.controls.phone.value
    }
    if (this.feature == 'Edit') {
      contact.id = this.contactId;
    }
    this.contactService.addContact(contact);
    this.router.navigate(['list-contact']);
  }
}
