import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddContactComponent } from './add-contact.component';
import { ContactService } from 'src/app/services/contact.service';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { IContact } from 'src/app/models/contact';
import { ListContactComponent } from '../list-contact/list-contact.component';
import { SortPipe } from 'src/app/pipes/sort.pipe';
import { SearchPipe } from 'src/app/pipes/search.pipe';
import { ContactDetailComponent } from 'src/app/components/contact-detail/contact-detail.component';

describe('AddContactComponent', () => {
  let component: AddContactComponent;
  let fixture: ComponentFixture<AddContactComponent>;
  let contactService: ContactService;
  let formBuilder: FormBuilder;
  let router: Router;
  let route: ActivatedRoute;
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddContactComponent,ListContactComponent,ContactDetailComponent,SortPipe, SearchPipe ],
      imports: [ReactiveFormsModule, RouterTestingModule.withRoutes([
        { path: 'list-contact', component: ListContactComponent}
    ])]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddContactComponent);
    component = fixture.componentInstance;
    contactService = TestBed.get(ContactService);
    formBuilder = TestBed.get(FormBuilder);
    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);
    location = TestBed.get(Location);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.contactForm).toBeTruthy();
  });

  describe('initializeForm', () => {
    it('should initialize form with existing values in case of Edit Form', () => {
      const contact: IContact =
      {
        id: '15656678788',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }
      component.initializeForm(contact);
      expect(component.contactForm.controls['firstName'].value).toEqual('Jay');
      expect(component.contactForm.controls['lastName'].value).toEqual('Visariya');
      expect(component.contactForm.controls['email'].value).toEqual('jayvisariya@gmail.com');
      expect(component.contactForm.controls['phone'].value).toEqual('8087556360');
    });

    it('should initialize form with blank values in case of Add Form', () => {
      const contact: IContact =
      {
        id: '15656678788',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }
      component.initializeForm(null);
      expect(component.contactForm.controls['firstName'].value).toEqual('');
      expect(component.contactForm.controls['lastName'].value).toEqual('');
      expect(component.contactForm.controls['email'].value).toEqual('');
      expect(component.contactForm.controls['phone'].value).toEqual('');
      component.initializeForm(contact);
    });
  });

  describe('addContact', () => {   
    it('should add contact when feature is add', () => {
      const contact: IContact =
      {
        id: '0',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360'
      }
      spyOn(component['contactService'], 'addContact');
      component.addContact();
      expect(component['contactService'].addContact).toHaveBeenCalled();
    });

    it('should add contact when feature is Edit', () => {
      const contact: IContact =
      {
        id: '0',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360'
      }
      spyOn(component['contactService'], 'addContact');
      component.addContact();
      expect(component['contactService'].addContact).toHaveBeenCalled();
    });
  });

});
