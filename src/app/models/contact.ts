
/**
 * Contains interfaces used in the application.
 */

export interface IContact {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    active?: boolean
}