import { TestBed } from '@angular/core/testing';

import { ContactService } from './contact.service';
import { IContact } from '../models/contact';

describe('ContactService', () => {
  let contactService: ContactService;
 
  beforeEach(() => {
    TestBed.configureTestingModule({})
    contactService = TestBed.get(ContactService);
  });

  it('should be created', () => {
    expect(contactService).toBeTruthy();
  });

  describe('setContacts', () => {
    it('should set contacts', () => {    
      let contacts: IContact[] = [{
        id: '0',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];
      localStorage.setItem('contacts',JSON.stringify(contacts));      
      expect(JSON.parse(localStorage.getItem('contacts')).length).toEqual(1);
      localStorage.removeItem('contacts');
    });
  });

  describe('getContacts', () => {
    it('should get contacts', () => {    
      let contacts: IContact[] = [{
        id: '0',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];
      localStorage.setItem('contacts',JSON.stringify(contacts));      
      expect(contactService.getContacts('all').length).toEqual(1);
      localStorage.removeItem('contacts');
    });
  });

  describe('addContact', () => {
    it('should add contact', () => {
      const contact: IContact = {
        id: '0',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      };
      let contacts: IContact[] = [];
      const originalContacts = spyOn(contactService, 'getContacts').withArgs('all').and.returnValue(contacts);
      spyOn(contactService, 'setContacts').withArgs(contacts).and.callFake(function () {
        contacts.push(contact);
      });
      contactService.addContact(contact);
      expect(contacts.length).toEqual(originalContacts.length + 1);
    });
  });

  describe('deleteContact', () => {
    it('should delete contact', () => {
      const contact: IContact = {
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      };
      let contacts: IContact[] = [{
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      },
      {
        id: '2',
        firstName: 'Ajay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];
      spyOn(contactService, 'getContacts').withArgs('all').and.returnValue(contacts);
      const index = spyOn(contactService, 'getIndex').withArgs(contacts, contact, 'id').and.returnValue(0);

      spyOn(contactService, 'setContacts').withArgs(contacts).and.callFake(function () {
        contacts[0].active = false;
      });
      contactService.deleteContact(contact);
      expect(contacts[0].active).toEqual(false);
    });
  });

  describe('getIndex', () => {
    it('should return index of contact from list of contacts', () => {
      const contact: IContact = {
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      };
      const contacts: IContact[] = [{
        id: '1',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }, {
        id: '2',
        firstName: 'Jay',
        lastName: 'Visariya',
        email: 'jayvisariya@gmail.com',
        phone: '8087556360',
        active: true
      }];
      const index = contactService.getIndex(contacts, contact, 'id');
      expect(index).toEqual(0);
    });
  });

});
