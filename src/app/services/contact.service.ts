import { Injectable } from '@angular/core';
import { IContact } from '../models/contact';

/***
 * Contact Service to do contacts related operations
 */
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor() { }

  /**
   * Sets contacts in Local Storage
   * Parameter: IContact[]
   */
  setContacts(contacts: IContact[]) {
    localStorage.setItem('contacts', JSON.stringify(contacts));
  }


  /**
   * returns IContact[] from Local Storage based on type.
   * Parameter: type ('active', 'inactive', 'all')
   */
  getContacts(type): IContact[] {
    const contactsFromSession: string = localStorage.getItem('contacts');
    const contacts: IContact[] = contactsFromSession ? JSON.parse(contactsFromSession) : [];
    switch (type) {
      case 'active':
        return contacts.filter((contact) => contact.active);
      case 'inactive':
        return contacts.filter((contact) => !contact.active);
      default:
        return contacts;
    }
  }

  /**
   * Deletes passed contact, updates storage and returns IContact[]
   * Parameter: IContact
   */
  deleteContact(contact: IContact): IContact[] {
    let contacts: IContact[] = this.getContacts('all');
    const index = this.getIndex(contacts, contact, 'id');
    contacts[index].active = false;
    this.setContacts(contacts);
    return contacts.filter((contact) => contact.active == true);
  }

  /**
   * Adds or Updates passed contact based on contact.id, updates storage and returns IContact[]
   * Parameter: IContact
   */
  addContact(contact: IContact): IContact[] {
    contact.active = true;
    let contacts: IContact[] = this.getContacts('all');
    if (contact.id == '0') { //Add Contact
      contact.id = (contacts.length + 1).toString();
      contacts.push(contact);
    } else { //Edit Contact
      const index = this.getIndex(contacts, contact, 'id');
      contacts[index] = contact;
    }
    this.setContacts(contacts);
    return contacts.filter((contact) => contact.active == true);
  }


  /**
   * returns index of the contact from list of contacts and param passed
   * Parameter: IContact[], IContact, Param 
   */
  getIndex(contacts: IContact[], contact: IContact, param: string): number {
    let index: number = -1;
    for (let i = 0; i < contacts.length; i++) {
      if (contacts[i][param] == contact[param]) {
        index = i;
        break;
      }
    }
    return index;
  }
}
