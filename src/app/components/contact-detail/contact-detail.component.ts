import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { contactDetailContent } from 'src/app/constants/content';
import { IContact } from 'src/app/models/contact';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {
  @Input('contact') contact: IContact;
  @Output() edit: EventEmitter<IContact> = new EventEmitter();
  @Output() delete: EventEmitter<IContact> = new EventEmitter();

  content: any = {};
  displayDeleleSureMessage: boolean;

  constructor() { }

  ngOnInit() {
    this.content = contactDetailContent;
    this.displayDeleleSureMessage = false;
  }

  /**
   * Emits the contact when user clicks on edit icon
   * Parameter: IContact
   */
  editContact(contact: IContact) {
    this.edit.emit(contact);
  }

   /**
   * Emits the contact when user clicks on delete icon
   * Parameter: IContact
   */
  deleteContact(contact: IContact) {
    this.displayDeleleSureMessage = false;
    this.delete.emit(contact);
  }

  /**
   * Shows delete message
   */
  showDeleteMessage() {
    this.displayDeleleSureMessage = true;
  }

  /**
   * Hides delete message
   */
  hideDeleteMessage() {
    this.displayDeleleSureMessage = false;
  }
}
