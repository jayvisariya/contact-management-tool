import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactDetailComponent } from './contact-detail.component';

describe('ContactDetailComponent', () => {
  let component: ContactDetailComponent;
  let fixture: ComponentFixture<ContactDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactDetailComponent);
    component = fixture.componentInstance;
    component.contact = {
      id: '15656678788',
      firstName: 'Jay',
      lastName: 'Visariya',
      email: 'jayvisariya@gmail.com',
      phone: '8087556360'
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.displayDeleleSureMessage).toEqual(false);
    expect(component.content).toBeTruthy();
  });

  describe('editContact', () => {
    it('should emit when editContact method is called', () => {
      spyOn(component.edit, 'emit');
      component.editContact(component.contact);
      expect(component.edit.emit).toHaveBeenCalled();
    });
  });

  describe('deleteContact', () => {
    it('should emit when deleteContact method is called', () => {
      spyOn(component.delete, 'emit');
      component.deleteContact(component.contact);
      expect(component.delete.emit).toHaveBeenCalled();
      expect(component.displayDeleleSureMessage).toEqual(false);
    });
  });

  describe('showDeleteMessage', () => {
    it('should show delete message', () => {
      component.showDeleteMessage();
      expect(component.displayDeleleSureMessage).toEqual(true);
    });
  });

  describe('hideDeleteMessage', () => {
    it('should hide delete message', () => {
      component.hideDeleteMessage();
      expect(component.displayDeleleSureMessage).toEqual(false);
    });
  });

});
