import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListContactComponent } from './views/contact-manager/list-contact/list-contact.component';
import { AddContactComponent } from './views/contact-manager/add-contact/add-contact.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-contact', pathMatch: 'full' },
  { path: 'list-contact', component: ListContactComponent },
  { path: 'add-contact/:id', component: AddContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
