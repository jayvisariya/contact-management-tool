import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListContactComponent } from './views/contact-manager/list-contact/list-contact.component';
import { AddContactComponent } from './views/contact-manager/add-contact/add-contact.component';
import { SearchPipe } from './pipes/search.pipe';
import { ContactDetailComponent } from './components/contact-detail/contact-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SortPipe } from './pipes/sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ListContactComponent,
    AddContactComponent,
    SearchPipe,
    ContactDetailComponent,
    SortPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
