import { Validators } from '@angular/forms';

/**
 * Contains all the validations used in the application.
 */
export const NAME_VALIDATOR = [Validators.required, Validators.maxLength(50), Validators.pattern(/^[a-zA-Z]+$/)];

export const EMAIL_VALIDATOR = [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)];

export const PHONE_VALIDATOR = [Validators.required, Validators.minLength(10), Validators.maxLength(12), Validators.pattern(/^[0-9]+$/)];
