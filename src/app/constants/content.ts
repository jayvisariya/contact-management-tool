
/**
 * Contains all the contents of the application
 */

export const commonContent = {
    APP_HEADING: 'Contact Management Tool'
};

export const addContactContent = {
    HEADING_ADD_CONTACT: 'Add Contact',
    HEADING_EDIT_CONTACT: 'Update Contact',
    BUTTON_ADD_TEXT: 'Add',
    BUTTON_CANCEL_TEXT: 'Cancel',
    BUTTON_EDIT_TEXT: 'Update',

    UPDATE_DIALOG_TITLE:'Update Contact',
    UPDATE_DIALOG_BODY:'Do you really want to update the contact?',
    UPDATE_DIALOG_BUTTON:'YES',       

    INPUT_FIRST_NAME_PLACEHOLDER: 'Enter First Name (Required)',
    INPUT_LAST_NAME_PLACEHOLDER: 'Enter Last Name (Required)',
    INPUT_EMAIL_PLACEHOLDER: 'Enter Email (Required)',
    INPUT_PHONE_PLACEHOLDER: 'Enter Phone Number (Required)',

    INPUT_FIRST_NAME_ERROR_REQUIRED: 'First name is required.',
    INPUT_FIRST_NAME_ERROR_VALID: 'Enter valid first name (Only alphabets are allowed).',
    INPUT_FIRST_NAME_ERROR_MAX_LIMIT: 'Only 50 Characters are allowed.',

    INPUT_LAST_NAME_ERROR_REQUIRED: 'Last name is required.',
    INPUT_LAST_NAME_ERROR_INVALID: 'Enter valid last name (Only alphabets are allowed).',
    INPUT_LAST_NAME_ERROR_MAX_LIMIT: 'Only 50 Characters are allowed.',

    INPUT_EMAIL_ERROR_REQUIRED: 'Email is required.',
    INPUT_EMAIL_ERROR_VALID: 'Enter valid email.',

    INPUT_PHONE_ERROR_REQUIRED: 'Phone number is required.',
    INPUT_PHONE_ERROR_VALID: 'Enter valid phone number (Only digits are allowed).',
    INPUT_PHONE_ERROR_MIN_MAX: 'Only 10 to 12 digits are allowed.',
};

export const listContactContent = {
    SEARCH_PLACEHOLDER: 'Search by name',
    BUTTON_NEW_CONTACT_TEXT: 'New Contact',
    NO_CONTACTS_AVAIALABLE_TEXT: 'No contacts available.'
};

export const contactDetailContent = {
    EMAIL_TAG_TEXT: 'Email: ',
    PHONE_TAG_TEXT: 'Phone: ',
    DELETE_DIALOG_TITLE:'Delete Contact',
    DELETE_DIALOG_BODY:'Do you want to delete this contact?',
    DELETE_DIALOG_BUTTON_YES:'YES',
    DELETE_DIALOG_BUTTON_NO:'NO'
};